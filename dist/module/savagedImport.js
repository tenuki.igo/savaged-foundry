import {logger, LOG_LVL} from "./log.js";
import {searchBestiary} from "./api.js";
import {sortBestiary} from "./utils.js";

export default class SavagedImport extends Dialog {
    static search_string = ""
    static beasts = []
    activateListeners(html) {
        super.activateListeners(html)
        const searchButton = html.find("button[id='beastSearch']")
        const searchField = html.find("input[id='searchString']")[0]
        const beastList = html.find("select[id='beastList']")[0]
        const loading = html.find("i[id='loadingSpinner']")[0]
        const importButton = html.find("button[data-button='Import']")[0]
        importButton.disabled = true
        searchField.addEventListener("keyup", function(event) {
            if ( event.keyCode === 13) {
                searchButton.trigger("click")
            }
        })
        searchButton.on("click", async () => {
            this.clearSelect(beastList)
            searchButton.disabled = true
            loading.style.display = ''
            logger(LOG_LVL.INFO, "got herer -tk")
            let api_key = localStorage.getItem("api-key")
            SavagedImport.search_string =  searchField.value
            SavagedImport.beasts = await searchBestiary(api_key, SavagedImport.search_string)
            logger(LOG_LVL.DEBUG, "found search results: " + JSON.stringify(SavagedImport.beasts))
            SavagedImport.sortedBeasts = await sortBestiary(SavagedImport.beasts)
            Object.keys(SavagedImport.sortedBeasts).forEach( bookName => {  // anyone who defends javascript as a language is a moron.
                let optionGroup = document.createElement("optgroup")
                optionGroup.label = bookName
                    + " (" + SavagedImport.sortedBeasts[bookName][0].bookPublisher + ", "
                    + SavagedImport.sortedBeasts[bookName][0].bookPublished + ")"
                SavagedImport.sortedBeasts[bookName].forEach( bestie => {
                    let option = document.createElement("option")
                    option.text = bestie.name
                    option.value = bestie.uuid
                    optionGroup.appendChild(option)
                })
                beastList.appendChild(optionGroup)
            })
            searchButton.disabled = false
            loading.style.display = 'none'
            importButton.disabled = false
        })
    }
    clearSelect(selectElement) {
        let list = Array.from(selectElement.children)
        for( let child of list) {
            child.remove()
        }
    }
}