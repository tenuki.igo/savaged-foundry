//import {logger, LOG_LVL} from "./log.js"
//const fetch = require('node-fetch')

// Note: above is to test fuction outside of browser.  I'm not fancy.
import {logger, LOG_LVL} from "./log.js";

export const fetchCharacters =
    async function fetchCharacters(api_key) {
        let url = "https://savaged.us/_api/auth/get-characters-generic-json"
        let options = {
            "method": "POST",
            "headers": {
                "Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
            },
            "body": "apikey=" + api_key
        }
        // Request Character List (POST https://savaged.us/_api/auth/get-characters-generic-json)
        let response = await fetch(url, options).catch((e) => {
            ui.notifications.warn("Error connecting to server: " + e)
        })
        let chars = await response.json().catch((e) => {
            ui.notifications.warn("Error retrieving results: " + e)
        })
        return chars
    }

export const fetchThisCharacter =
    async function fetchThisCharacter(api_key, uuid) {
        let url = "https://savaged.us/_api/auth/get-character-by-uuid-generic-json"
        let options = {
            "method": "POST",
            "headers": {
                "Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
            },
            "body": "apikey=" + api_key + "&uuid=" + uuid
        }
        // Request Character List (POST https://savaged.us/_api/auth/get-characters-generic-json)
        let response = await fetch(url, options).catch((e) => {
            ui.notifications.warn("Error connecting to server: " + e)
        })
        let character = await response.json().catch((e) => {
            ui.notifications.warn("Error retrieving result: " + e)
        })
        return character
    }

export const searchBestiary =
    async function searchBestiary(api_key, searchString) {
        let url = "https://savaged.us/_api/auth/search-bestiary-generic-json"
        let options = {
            "method": "POST",
            "headers": {
                "Content-Type": "application/json; charset=utf-8"
            },
            "body": JSON.stringify({search: searchString, apikey: api_key})
        }
        // Request Character List (POST https://savaged.us/_api/auth/search-bestiary-generic-json)
        let response = await fetch(url, options).catch((e) => {
            console.log(e)
            ui.notifications.warn("Error connecting to server: " + e)
        })
        console.log("test")
        let chars = await response.json().catch((e) => {
            console.log(e)
            ui.notifications.warn("Error retrieving results: " + e)
        })
        return chars
    }

export const testConnection =
    async function testConnection(api_key) {
        let url = "https://savaged.us/_api/auth/whoami"
        let options = {
            "method": "POST",
            "headers": {
                "Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
            },
            "body": "apikey=" + api_key
        }
        // Request whoami (POST https://savaged.us/_api/auth/whoami)
        let response = await fetch(url, options).catch((e) => {
            console.log(e)
            ui.notifications.warn("Network error connecting to server: " + e)
            return false
        })
        if (response.status >= 200 && response.status <= 299) {
            let tmp = await response.json()
            let data = JSON.parse(tmp)
            logger(LOG_LVL.INFO, JSON.stringify(data))
            if (data.name.length > 2) {
                ui.notifications.info("API key tested successful! Setting it local storage.")
                localStorage.setItem("api-key", api_key)
                logger(LOG_LVL.DEBUG, "Set Savaged.Us API Key to browser local storage")
                return true
            } else {
                ui.notifications.warn("Entered API key tested not valid with Savaged.US.")
                logger(LOG_LVL.INFO, 'Error setting API Key.')
                return false
            }
        } else {
            ui.notifications.warn("Received error response from remote server: " + response.status + " - " + response.statusText)
            return false
        }
    }
// export const fetchVehicles = async function fetchVehicles(api_key) {
//
// }

// To Test: uncomment the node fetch import line, the test() call, comment out the exports
async function test() {
    let test = await fetchCharacters("2845xNDgyNDVjYTAtYmZlNS0xMWVhLWE3OTItYzdjMWMwNDY4MDQ4")
    console.log("fetchCharacters" + test)
    test = await searchBestiary("2845xNDgyNDVjYTAtYmZlNS0xMWVhLWE3OTItYzdjMWMwNDY4MDQ4", "cat")
    console.log("searchBestiary: " + test)
}

//test()