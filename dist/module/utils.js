import {LOG_LVL, logger} from "./log.js";

export const getSelectedOptions = function getSelectedOptions(selectElement) {
    let selectedOptions = []
    for(let i=0; i< selectElement.options.length; i++) {
        if(selectElement.options[i].selected) {
            selectedOptions.push(selectElement.options[i].value)
        }
    }
    return selectedOptions
}

export const getCurrentActorsUUID = function getCurrentActorsUUID(actors) {
    let data = {}
    actors.forEach(actor => {
        if (actor.data.flags.importInfo && actor.data.flags.importInfo.uuid) {
            data[actor.data.flags.importInfo.uuid] = actor.data.flags.importInfo
            data[actor.data.flags.importInfo.uuid].id = actor.id
        } else {
            logger(LOG_LVL.DEBUG, "actor not an import with uuid, skipping")
        }
    })
    return data
}

export const sortCharacters = function sortCharacters(characters, uuidLookup) {
    let data = {
        "New": [],
        "Synched": [],
        "Update": []
    }
    characters.forEach(char => {
        // foundry character
        if (char.uuid in uuidLookup) {
            let foundryChar = uuidLookup[char.uuid]// check if character is already imported
            //TODO: compare time instead of string and only update if savaged is newer ( otherwise alert )
            if (foundryChar.updated !== char.updatedDate) {             // if yes check if character is out of date
                data["Update"].push(char)                                // if yes put in update category
            } else {
                data["Synched"].push(char) // otherwise place in synced category
            }
        } else {
            data["New"].push(char) // place in new category
        }
    })
    return data;
}

export const sortBestiary = async function sortBestiary (besties) {
    let sortedBesties = {}
    besties.forEach( best => {
        logger(LOG_LVL.DEBUG, "bestiary entry: " + JSON.stringify(best))
        if ( best.bookName in sortedBesties ) {
            sortedBesties[best.bookName].push(best)
        } else {
            sortedBesties[best.bookName] = [best]
        }
    })
    return sortedBesties
}