# Savaged.US -> Foundry VTT character importer module

Module to import Savage World Adventure Edition characters created in the amazing savaged.us character generator into 
FloRad's excellent SWADE system for FoundryVTT.

[[_TOC_]]

There are two levels of module functionality tied to wether or not you have a paid account at savaged.us

### Free Savaged.US account
  - File based import of the generic json format provided to free savaged.us accounts.

### Wildcard Savaged.US account
  - The above
  - PLUS direct imports of your savaged.us saved characters from within FoundryVTT.
  - PLUS direct imports of bestiary entries from within FoundryVTT.

## Introduction

Savaged.us is a fantastic character creator for Savaged Worlds, and a lot more.  It is feature rich and 
has the ability for GMs to specify their setting specific set of power, items, ect and allows you to customize
them with custom items, powers, skills, etc.  You then share those with your players and voila!! 

## Installation:

- Normally via the module manager.
- ~or~
    - `git checkout https://gitlab.com/tenuki.igo/savaged-foundry` 
    - `cd savaged-foundry`
    - Edit your `foundryconfig.json` file to update to your data directory
    - `npm install`
    - `npm run build`
    - `npm link` // This will create a link to this project in your foundry data directory
    
The module should now show up in your foundry and can be added to any SWADE games you have.

## Use

File base sharing: 

GM: 
1) Add this module to your swade world.
1) Build your SWADE setting specifics using https://savaged.us.
1) Share those with your players.

Players:
1) Click on 'use these settings' on the setting share link your GM gives you.
1) Create your character within that world and it's special quirks.
1) export as Generic JSON.
1) import that file using the _'import character'_ button in the 
1) check it over and add a character portrait

[More details at the Wiki with pictures](https://gitlab.com/tenuki.igo/savaged-foundry/-/wikis/How-to-Import-a-Character)
    
## Contributing:

I do occasionally accept contributions as merge requests.  However, you might want to check with me first
before you do a lot of work...

Contribututions must come as merge requests on the develop branch, all others will be rejected without comment.  
Please follow my general coding style, and code formatting, ie no semi-colons, etc.  I don't care what you think the 
right way is, you can code wrong on your own projects.  ;P

I use the gitlab issue board and milestones.  Please only contribute issues slated for the next milestone, and put 
the issue number in the name of the merge request.  Thanks!
