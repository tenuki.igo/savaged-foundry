import {logger, LOG_LVL} from "./log.js";

export const importCharacterData = function (inData, options, inType="character") {
    // uploadImages(inData)
    logger(LOG_LVL.DEBUG, "options: " + options)
    logger(LOG_LVL.DEBUG, "character data: " + JSON.stringify(inData))
    // TODO: rework this logic based on discord conversation with FloRad and Gordon
    let isWild = false
    if (typeof inData.wildcard !== 'undefined') {  // javascript is truly a pile of shit language
        isWild = true
    }
    if (inData.alliedExtras && inData.alliedExtras.length > 0) {
        inData.alliedExtras.forEach(async ally => {
            let updatedActor = game.actors.find(actor => actor.data.type === 'npc' && typeof actor.data.flags.importInfo !== 'undefined' && typeof actor.data.flags.importInfo.uuid !== 'undefined' && actor.data.flags.importInfo.uuid === ally.uuid)
            let importedActor = {}
            if (updatedActor !== null) {
                importedActor = await updatedActor.update(importCharacterData(ally,{"ownerUUID": inData.uuid},"npc"))
            } else {
                importedActor = await Actor.create(importCharacterData(ally,{"ownerUUID": inData.uuid},"npc"))
            }
            //Sets permissions to match ally's owner actor's permissions
            let owner = game.actors.find(actor => actor.data.type === 'character' && typeof actor.data.flags.importInfo !== 'undefined' && typeof actor.data.flags.importInfo.uuid !== 'undefined' && actor.data.flags.importInfo.uuid === inData.uuid)
            if (owner) {
                importedActor.data.permission = owner.data.permission
            }
        })
    }
    let ownerUUID = ''
    if (typeof options.ownerUUID !== 'undefined') {
        ownerUUID = options.ownerUUID
    }
    let outData = {
        name: inData.name,
        type: inType,
        img: inData.image, // | "icons/svg/mystery-man.svg",
        token: mapToken(inData, options, inType),
        sort: 12000,
        data: {
            attributes: mapAttributes(inData.attributes),
            stats: mapStats(inData),
            details: mapDetails(inData),
            powerPoints: mapPowerPoints(inData),
            fatigue: mapFatigue(inData),
            wounds: mapWounds(inData),
            advances: mapAdvances(inData),
            bennies: mapBennies(inData),
            wildcard: isWild
        },
        auotCalc: {
            "toughness": true
        },
        items: mapItems(inData),
        flags: {
            importInfo: {
                app: 'Savaged.US',
                method: options.method,
                appVersion: inData.appVersion,
                importer: 'savaged-foundry module',
                created: inData.createdDate,
                updated: inData.updatedDate,
                uuid: inData.uuid,
                ownerUUID: ownerUUID
            }
        }                                       // TODO: populate flags
    }
    logger(LOG_LVL.DEBUG, "!! mapped Actor Data: " + JSON.stringify(outData))
    return outData
}

// TODO: finish this and trigger it to make the images available on the server. Maybe put this outside in it's own file
async function uploadImages(inData) {
    fetch(inData.image)
        .then(response => response.blot())
        .then(images => {
            // TODO: error handle much?
            FilePicker.upload('world', '.', file, {}).then(logger(LOG_LVL.DEBUG, "uploadImages: complete "))
        })
}

function writeDescription(item) {
    let description = ''
    let source = ''
    if (item.takenFrom) { //If the new takenFrom property exists, use it.
        source = "takenFrom"
    } else {
        source = "book_name_page" //Default to book_name_page otherwise
    }
    if (item.description) {
        if (item[source]) {
            description = item.description + '<p>See '+ item[source] + ' for more details.</p>'
        } else {
            description = item.description
        }
    } else {
        if (item[source]) {
            description = '<p>See '+ item[source] + ' for more details.</p>'
        } else {
            description = '<p>No description or reference provided.</p>'
        }
    }
    return description
}

// TODO: For implementation after skill-based additional actions are added to SWADE.
function mapSkillActions(inData, skill, actionName) {
    return {
        name: actionName,
        type: 'skill',
        skill: skill,
        mod: inData.skills.find(mod => name === skill)
    }
}

function mapToken(inData, options, inType) {
    let tokenData = {}
    if (game.settings.get("savaged-foundry", "hasVision") && (inType === 'character' || typeof options.ownerUUID !== 'undefined')) {
        tokenData.vision = true
    }
    if (inType === 'character' || typeof options.ownerUUID !== 'undefined') {
        tokenData.disposition = 1
        tokenData.actorLink = true
    }
    if (!inData.imageToken)
        inData.imageToken = "icons/svg/mystery-man.svg"
    tokenData.name = inData.name
    tokenData.img = inData.imageToken // | "icons/svg/mystery-man.svg"//  //Note this is not yet implemented on savaged.us - upcoming
    return tokenData
}

function mapItems(inData) {
    return mapSkills(inData)
        .concat(mapArmor(inData))
        .concat(mapWeapons(inData))
        .concat(mapGear(inData))
        .concat(mapCyberware(inData))
        .concat(mapEdges(inData))
        .concat(mapHindrances(inData))
        // .concat(mapAbilities(inData))  // TODO: no place for them in Foundry SWADE
        .concat(mapPowers(inData))
}

function mapPowers(inData) {
    let pows = []
    //logger(LOG_LVL.DEBUG, "mapPowers: finding magic " + inData.abs + ' : ' + JSON.stringify(inData.abs))
    inData.abs.forEach(abs => {
        // TODO: move powerpoints and other stuff here for effeciency
        //logger(LOG_LVL.DEBUG, "mapPowers: " + JSON.stringify(abs))
        let arcane = abs.arcaneSkill
        let isEquipped = false
        abs.powers.forEach(pow => {
            inData.weapons.forEach(weapon => {
                if (weapon.name === pow.name && weapon.notes.startsWith('Power, Power Points: ')) {
                    isEquipped = true
                }
            })
            pows.push({
                name: pow.name,
                type: "power",
                img: "systems/swade/assets/icons/power.svg",
                data: {
                    description: pow.summary,
                    pp: pow.powerPoints,
                    damage: pow.damage,
                    notes: pow.description,
                    range: pow.range,
                    duration: pow.duration,
                    arcane: arcane, //,rank: ""  TODO: get this info in the export data
                    equipped: isEquipped,
                    actions: {
                        skill: arcane,
                        skillMod: inData.skills.find(mod => name === arcane)
                    }
                }
            })
        })
    })
    //logger(LOG_LVL.DEBUG, "mapPowers: " + JSON.stringify(pows))
    return pows
}

// TODO: decide or at least give the option to treat cyberware differently - ie  Gear | Edge | power - this chooses gear
function mapCyberware(inData) {
    let cyber = []
    if (typeof inData.cyberware !== 'undefined') {
        logger(LOG_LVL.DEBUG, "mapCyberware: " + JSON.stringify(inData.cyberware))
        inData.cyberware.forEach(item => {
            logger(LOG_LVL.DEBUG, "mapCyberware: item: " + JSON.stringify(item))
            let description = writeDescription(item)
            cyber.push({
                name: item.name,
                type: "gear",
                img: "modules/savaged-foundry/assets/icons/cyberware.svg",
                data: {
                    description: "<p><b>Cyberware:</b> Must always be worn as it is embedded!!</p><p><b>Strain:</b> " + item.strain + "</p>" + description, //Florad is working on something to make items potentially unequippable
                    weight: 0,
                    strain: item.strain,
                    notes: item.notes,
                    quantity: item.quantity,
                    price: item.cost,
                    equippable: true, //Florad is working on something to make items potentially unequippable
                    equipped: true
                }
            })
        })
    }
    return cyber
}

// TODO: find a place for these.
function mapAbilities(inData) {
    let abs = []

    inData.abilities.forEach(ab => {
        logger(LOG_LVL.DEBUG, "mapAbilities: " + JSON.stringify(ab))
        abs.push({
            name: ab.name,
            type: "ability",
            img: "systems/swade/assets/icons/hindrance.svg",
            data: {
                description: ab.description+'<hr>'+ab.notes,
                notes: ab.note,
                major: ab.major
            }
        })
    })
    return abs
}

function mapHindrances(inData) {
    let hinds = []
    inData.hindrances.forEach(hi => {
        logger(LOG_LVL.DEBUG, "mapHindrances: " + JSON.stringify(hi))
        let description = writeDescription(hi)
        hinds.push({
            name: hi.name,
            type: "hindrance",
            img: "systems/swade/assets/icons/hindrance.svg",
            data: {
                description: description,
                notes: hi.note,
                major: hi.major
            }
        })
    })
    return hinds
}

function mapEdges(inData) {
    let eds = []
    inData.edges.forEach(edge => {
        logger(LOG_LVL.DEBUG, "mapEdges: edge: " + JSON.stringify(edge))
        let description = writeDescription(edge)
        eds.push({
            name: edge.name,
            type: "edge",
            img: "systems/swade/assets/icons/edge.svg",
            data: {
                description: description,
                notes: edge.note,
                isArcaneBackground: edge.name.includes("Arcane Background")
            }
        })
    })
    logger(LOG_LVL.DEBUG, "mapGear: Edges: " + JSON.stringify(eds))
    return eds
}

function mapGear(inData) {
    let gear = []
    logger(LOG_LVL.DEBUG, "mapGear: " + JSON.stringify(inData.gear))
    inData.gear.forEach(item => {
        if (item.name !== ''){
            logger(LOG_LVL.DEBUG, "mapGear: item: " + JSON.stringify(item))
            let description = writeDescription(item)
            gear.push({
                name: item.name,
                type: "gear",
                img: "systems/swade/assets/icons/gear.svg",
                data: {
                    description: description,
                    weight: item.weight,
                    notes: item.notes,
                    quantity: item.quantity,
                    price: 0,
                    equippable: true,
                    equipped: item.equipped
                }
            })
        }
    })
    return gear
}

function mapWeapons(inData) {
    let weapons = []
    logger(LOG_LVL.DEBUG, "mapWeapons: " + JSON.stringify(inData.weapons))
    inData.weapons.forEach(item => {
        logger(LOG_LVL.DEBUG, "mapWeapons: item: " + JSON.stringify(item))
        let name = item.name
        let description = writeDescription(item)
        let dmgRoll = item.damage.replace(/Str/gi, '@str')
        let range = item.range
        let isEquipped = item.equipped
        let isEquippable = true
        let isUnequippable = true
        let skill = ''
        //let actionName = ''
        let actions = {}
        //actions.additional = {}
        //Skip weapons that are traditional Arcane Powers
        if (!item.notes.startsWith('Power, Power Points: ')) {
            if (item.notes.includes('Innate Attack')) {
                isUnequippable = false
                isEquippable = true
                isEquipped = true
            }
            if (range == "Melee") { // messes with the way SWADE detects ranged vs melee weapons ( blank range )
                range = ''
                skill = 'Fighting'
                actions.skill = skill
                actions.skillMod = inData.skills.find(mod => name === skill)
                //actionName = skill
                //actions.additional[actionName] = mapSkillActions(inData, skill, actionName)
            } else {
                // TODO: Implement skill-based additional actions after support is added to SWADE.
                if (item.thrown === true) {
                    if (item.profiles && item.profiles.length && item.profiles[0].usable_in_melee === 1) {
                        name = item.name + ' (throwing)'
                    }
                    skill = 'Athletics'
                    actions.skill = skill
                    actions.skillMod = inData.skills.find(mod => name === skill)
                    //actionName = skill + '(Throwing)' 
                    //actions.additional[actionName] = mapSkillActions(inData, skill, actionName)
                } else {
                    skill = 'Shooting'
                    actions.skill = skill
                    actions.skillMod = inData.skills.find(mod => name === skill)
                    //actionName = skill
                    //actions.additional[actionName] = mapSkillActions(inData, skill, actionName)
                }
            }
            if (item.notes.startsWith('Power')) {
                let arcaneSkill = ''
                inData.abs.forEach(ab => {
                    ab.powers.forEach(power => {
                        if (power.name === item.name) {
                            skill = ab.arcaneSkill
                        }
                    })
                })
            }
            logger(LOG_LVL.DEBUG, "DamageRoll: " + dmgRoll)
            weapons.push({
                name: name,
                type: "weapon",
                img: "systems/swade/assets/icons/weapon.svg",
                data: {
                    description: description,
                    weight: item.weight,
                    notes: item.notes,
                    quantity: item.quantity,
                    price: 0,
                    equippable: isEquippable,
                    unequippable: isUnequippable,
                    equipped: isEquipped,
                    range: range,
                    damage: dmgRoll,
                    rof: item.rof,
                    ap: item.ap,
                    shots: item.shots,
                    minStr: 0,
                    armor: item.armor,
                    attribute: item.attribute,
                    actions: {
                        skill: skill,
                        skillMod: inData.skills.find(mod => name === skill)
                    }
                }
            })
            // If a thrown item can also be used in melee, push a second copy
            if (item.profiles && item.profiles.length && item.profiles[0].usable_in_melee === 1) {
                skill = 'Fighting'
                name = item.name + ' (melee)'
                weapons.push({
                    name: name,
                    type: "weapon",
                    img: "systems/swade/assets/icons/weapon.svg",
                    data: {
                        description: description,
                        weight: item.weight,
                        notes: item.notes,
                        quantity: item.quantity,
                        price: 0,
                        equippable: isEquippable,
                        unequippable: isUnequippable,
                        equipped: isEquipped,
                        range: '',
                        damage: dmgRoll,
                        rof: item.rof,
                        ap: item.ap,
                        shots: item.shots,
                        minStr: 0,
                        armor: item.armor,
                        attribute: item.attribute,
                        actions: {
                            skill: skill,
                            skillMod: inData.skills.find(mod => name === skill)
                        }
                    }
                })
            }
            //Add an additional Super Power Ranged Attack with the Athletics skill for the possibility of a thrown trapping. This will go away once multiple skill actions are supported.
            if (item.notes.includes('Super Power') && item.range !== 'Melee' && item.thrown !== true) {
                skill = 'Athletics'
                name = item.name + ' (thrown)'
                weapons.push({
                    name: name,
                    type: "weapon",
                    img: "systems/swade/assets/icons/weapon.svg",
                    data: {
                        description: description,
                        weight: item.weight,
                        notes: item.notes,
                        quantity: item.quantity,
                        price: 0,
                        equippable: isEquippable,
                        unequippable: isUnequippable,
                        equipped: isEquipped,
                        range: item.range,
                        damage: dmgRoll,
                        rof: item.rof,
                        ap: item.ap,
                        shots: item.shots,
                        minStr: 0,
                        armor: item.armor,
                        attribute: item.attribute,
                        actions: {
                            skill: skill,
                            skillMod: inData.skills.find(mod => name === skill)
                        }
                    }
                })
            }
        }
    })
    logger(LOG_LVL.DEBUG, "mapWeapons: " + JSON.stringify(weapons))
    return weapons
}

function mapArmor(inData) {
    let armor = []
    let hasArmorAbility = false
    logger(LOG_LVL.DEBUG, "mapArmor: " + JSON.stringify(inData.armor))
    inData.abilities.forEach(ability => {
        if (ability.name.startsWith('Armor +')) {
            hasArmorAbility = true
        }
    })
    if (inData.armor.length === 0 && inData.armorValue > 0 && hasArmorAbility === true) {
        let armorName = 'Armor +' + inData.armorValue
        let isEquipped = true
        let isEquippable = true
        let isUnequippable = false
        let isNaturalArmor = true
        let description = 'Inherent Armor'
        inData.abilities.forEach(ability => {
            if (ability.name.includes('Armor')) {
                description = ability.description
            }
        })
        armor.push({
            name: armorName,
            type: "armor",
            img: "systems/swade/assets/icons/armor.svg",
            data: {
                description: description,
                quantity: 1,
                equippable: isEquippable,
                unequippable: isUnequippable,
                equipped: isEquipped,
                armor: inData.armorValue,
                isNaturalArmor: isNaturalArmor,
                locations: {
                    head: true,
                    torso: true,
                    arms: true,
                    legs: true
                }
            }
        })
    } else {
        inData.armor.forEach(item => {
            logger(LOG_LVL.DEBUG, "mapArmor: item: " + JSON.stringify(item))
            let armorName = item.name
            // Since Armor in Foundry can be unequipped, there's no real need for (Unarmored) to be added. If a special ability exists, it's accounted for below.
            if (armorName !== '(Unarmored)' || (armorName === '(Unarmored)' && item.armor > 0)) {
                let isEquipped = item.equipped
                let isEquippable = true
                let isUnequippable = true
                let armorValue = item.armor
                let isNaturalArmor = false
                let description = writeDescription(item)
                if (armorName === '(Unarmored)' && item.armor > 0) {
                    armorName = 'Natural Armor'
                    isEquipped = true
                    isEquippable = true
                    isUnequippable = false
                    isNaturalArmor = true
                }
                armor.push({
                    name: armorName,
                    type: "armor",
                    img: "systems/swade/assets/icons/armor.svg",
                    data: {
                        description: description,
                        weight: item.weight,
                        quantity: item.quantity,
                        price: item.cost,
                        equippable: isEquippable,
                        unequippable: isUnequippable,
                        equipped: isEquipped,
                        minStr: item.minStr,
                        armor: armorValue,
                        attribute: item.attribute,
                        isNaturalArmor: isNaturalArmor,
                        locations: {
                            head: item.coversHead,
                            torso: item.coversTorso,
                            arms: item.coversArms,
                            legs: item.coversLegs
                        }
                    }
                })
            }
        })
    }
    logger(LOG_LVL.DEBUG, "mapArmor: " + JSON.stringify(armor))
    return armor
}

function mapSkills(inData) {
    let skills = []
    logger(LOG_LVL.DEBUG, "mapSkills: inSkills: " + JSON.stringify(inData.skills))
    inData.skills.forEach(skill => {
        logger(LOG_LVL.DEBUG, "mapSkills: skill: " + JSON.stringify(skill))
        skills.push({
            name: skill.name,
            type: "skill",
            img: "systems/swade/assets/icons/skill.svg",
            data: {
                description: "", //Removed until source and page references are added via Savaged.us
                attribute: skill.attribute,
                die: {
                    sides: skill.dieValue,
                    modifier: skill.mod
                },
                "wild-die": {
                    sides: 6
                }
            }
        })
    })
    skills.push({
        name: "Unskilled Attempt",
        type: "skill",
        img: "systems/swade/assets/icons/skill.svg",
        data: {
            description: "See SWADE p.89",
            attribute: "",
            die: {
                sides: 4,
                modifier: -2
            },
            "wild-die": {
                sides: 6
            }
        }
    })
    logger(LOG_LVL.DEBUG, "mapSkills: " + JSON.stringify(skills))
    return skills
}

function mapAdvances(inData) {
    let advancesDetailsTable = document.createElement('table'),
        tableInnerHTML = ''
    advancesDetailsTable.setAttribute('cellpadding','3')
    inData.advances.forEach(advance => {
        switch (advance.number) {
            case 1:
                tableInnerHTML += '<tbody><tr><th colspan="2">Novice</th></tr>'
                tableInnerHTML += '<tr><td>' + advance.number + '</td><td>' + advance.description + '</td></tr>'
                break;
            case 4:
                tableInnerHTML += '<tbody><tr><th colspan="2">Seasoned</th></tr>'
                tableInnerHTML += '<tr><td>' + advance.number + '</td><td>' + advance.description + '</td></tr>'
                break;
            case 8:
                tableInnerHTML += '<tbody><tr><th colspan="2">Veteran</th></tr>'
                tableInnerHTML += '<tr><td>' + advance.number + '</td><td>' + advance.description + '</td></tr>'
                break;
            case 12:
                tableInnerHTML += '<tbody><tr><th colspan="2">Heroic</th></tr>'
                tableInnerHTML += '<tr><td>' + advance.number + '</td><td>' + advance.description + '</td></tr>'
                break;
            case 16:
                tableInnerHTML += '<tbody><tr><th colspan="2">Legendary</th></tr>'
                tableInnerHTML += '<tr><td>' + advance.number + '</td><td>' + advance.description + '</td></tr>'
                break;
            default:
                tableInnerHTML += '<tr><td>' + advance.number + '</td><td>' + advance.description + '</td></tr>'
                break;
        }
        switch (advance.number) {
            case 3:
            case 7:
            case 11:
            case 17:
            case inData.advancesCount:
                tableInnerHTML += '</tbody>'
                break;
        }
    })
    advancesDetailsTable.innerHTML = tableInnerHTML
    let adv = {
        value: inData.advancesCount,
        rank: inData.rankName,
        details: advancesDetailsTable.outerHTML
    }
    logger(LOG_LVL.DEBUG, "mapAdvances:" + JSON.stringify(adv))
    return adv
}

function mapFatigue(inData) {
    let fat = {
        value: inData.fatigue,
        min: 0,
        max: inData.fatigueMax
    }
    logger(LOG_LVL.DEBUG, "mapFatigue: " + JSON.stringify(fat))
    return fat
}

function mapWounds(inData) {
    let wounds = {
        value: inData.wounds,
        min: 0,
        max: inData.woundsMax
    }
    logger(LOG_LVL.DEBUG, "mapWounds: " + JSON.stringify(wounds))
    return wounds
}

// TODO: find them in the import - need character with ppts
function mapPowerPoints(inData) {
    let pp = {value: 0, max: 0}
    if (inData.abs.length > 1) {
        inData.abs.forEach(abs => {
            pp[abs.arcaneSkill] =
                {
                    value: abs.powerPointsCurrent,
                    max: abs.powerPointsMax
                }
        })
        logger(LOG_LVL.DEBUG, "-tk")
    } else if (inData.abs.length > 0) {
        pp = {
            value: inData.abs[0].powerPointsCurrent,
            min: 0,
            max: inData.abs[0].powerPointsMax
        }
        logger(LOG_LVL.DEBUG, "tk")
    }
    return pp
}

function mapDetails(inData) {
    let descriptionHTML = ""
    let backgroundHTML = ""
    let specAbilitiesText = ""
    let specialAbilitiesHTML = ""
    let imageHTML = ""
    let importMetadata = {
        url: inData.savagedUsShareURL,
        source: inData.bookName,
        publisher: inData.bookPublisher,
        citation: '',
        uuid: inData.uuid,
        appVersion: inData.appVersion,
        createdDate: inData.createdDate,
        updatedDate: inData.updatedDate,
        importedDate: function() { 
            let now = new Date()
            return now.toUTCString()
        }
    }
    if (inData.abilities.length) {
            inData.abilities.forEach(ab => {
            logger(LOG_LVL.DEBUG, "iterating racial abilities; " + JSON.stringify(ab))
            specAbilitiesText = specAbilitiesText.concat('<li><span class="bold">' + ab.name + ':</span> ' + ab.description + '</li>')
        })
    }
    if (inData.description !== '') {
        descriptionHTML = '<h3 class="bold">Description:</h3><p style="margin: 5px 5px 20px">' +
                inData.description +
                '</p>'
    }
    if (inData.background !== '') {
        backgroundHTML = '<h3 class="bold">Background:</h3><p style="margin: 5px 5px 20px">' +
                inData.background +
                '</p>'
    }
    if (specAbilitiesText !== '') {
        specialAbilitiesHTML = '<h3 class="bold">Special Abilities:</h3><p style="margin: 5px 5px 20px"><ul>' +
                specAbilitiesText +
                '</ul></p>'
    }
    if (game.settings.get("savaged-foundry", "addImage") && inData.image !== '') {
        imageHTML = '<figure class="savagedus-portrait"><img src="' + inData.image + '"/><figcaption class="caption">Portrait of ' + inData.name + '</figcaption></figure>'
    }
    if (importMetadata.source !== '') {
        if (importMetadata.publisher !== '') {
            importMetadata.citation = '<p>Originally published in <cite>' + importMetadata.source + '</cite> by ' + importMetadata.publisher + '.<p>'
        } else {
            importMetadata.citation = '<p>Originally published in <cite>' + importMetadata.source + '</cite>.<p>'
        }
    }
    if (importMetadata.url === '') {
        importMetadata.url = 'https://savaged.us/'
    }
    if (importMetadata.createdDate === null) {
        importMetadata.createdDate = "unknown"
    }
    if (importMetadata.updatedDate === null) {
        importMetadata.updatedDate = "unknown"
    }
    let details = {
        biography: {
            value: descriptionHTML +
                backgroundHTML +
                specialAbilitiesHTML +
                imageHTML +
                '<footer class="savagedInfo">' +
                importMetadata.citation +
                '<p>Imported from <a href="' + importMetadata.url + '">Savaged.us</a>!</p>' +
                '<h4>Import Details</h4>' +
                '<ul>' +
                '<li>uuid: <code>' + importMetadata.uuid + '</code></li>' +
                '<li>Savaged.us version: <code>' + importMetadata.appVersion + '</code></li>' +
                '<li>importer: <a href="https://foundryvtt.com/packages/savaged-foundry/">Savaged-Foundry Importer Foundry module</a></li>'+
                '<li>created on savaged.us: <code>' + importMetadata.createdDate + '</code></li>' +
                '<li>updated on savaged.us: <code>' + importMetadata.updatedDate + '</code></li>' +
                '<li>imported on: <code>' + importMetadata.importedDate() + '</code></li>' +
                '</ul>' +
                '<p>Savaged.us and its logo are copyrighted by Alliante Entertainment.</p>' +
                '<footer>'
        },
        species: {
            name: inData.race
        },
        currency: inData.wealth,
        autoCalcToughness: true
    }
    logger(LOG_LVL.DEBUG, "mapDetails: " + JSON.stringify(details))
    return details
}

function mapStats(inData) {
    let stats = {
        speed: {
            sprintDie: inData.runningDie,
            value: inData.paceTotal
        },
        toughness: {
            value: inData.toughnessBase,
            armor: inData.armorValue,
            modifier: inData.toughnessMod - inData.size
        },
        parry: {
            value: inData.parryTotal,
            modifier: inData.parryMod
        },
        size: inData.size
    }
    logger(LOG_LVL.DEBUG, "mapStats: " + JSON.stringify(stats))
    return stats
}

function mapAttributes(attr) {
    logger(LOG_LVL.DEBUG, "attr: " + JSON.stringify(attr))
    logger(LOG_LVL.DEBUG, "attr agility: " + JSON.stringify(attr.find(e => e.name === "agility")))
    logger(LOG_LVL.DEBUG, "attr agility dice: " + JSON.stringify(attr.find(e => e.name === "agility").dieValue))
    let data = {}
    attr.forEach(attribute => {
        data[attribute.name] = {}
        data[attribute.name].die = {}
        data[attribute.name].die.sides = attribute.dieValue
        if (attribute.name === "smarts" && attribute.value.includes('(A)')) {
            data[attribute.name].animal = true
        }
        data[attribute.name].die.modifier = attribute.mod
        data[attribute.name]["wild-die"] = {}
        data[attribute.name]["wild-die"].sides = 6
    })
    logger(LOG_LVL.DEBUG, "mapAttributes: " + data)
    return data
}

function mapBennies(inData) {
    let bennies = {
        value: inData.bennies,
        max: inData.benniesMax
    }
    return bennies
}
