export const registerSettings = function () {
    // Register any custom module settings here
    // game.settings.register("savaged-foundry", "settingName", {
    //     name: "Default World Setting Name (Not yet implemented):",
    //     hint: "Set the name of your World Setting. Used to create the custom compendiums for items not in core rules during import.",
    //     type: String,
    //     default: "Savaged Import",
    //     scope: "world",
    //     config: true
    // })
    //
    // game.settings.register("savaged-foundry", "createCompendiums", {
    //     name: "Create Compendiums for non-core Settings (Not yet implemented):",
    //     hint: "Create custom compendiums for items not in core rules during import.",
    //     type: Boolean,
    //     default: true,
    //     scope: "world",
    //     config: true
    // })
    //
    // game.settings.register("savaged-foundry", "validateInput", {
    //     name: "Validate input file before loading: (Not yet Implemented)",
    //     hint: "Turning this off could speed up import at the sacrifice of data cleanliness",
    //     type: Boolean,
    //     default: true,
    //     scope: "world",
    //     config: true
    // })

    game.settings.register("savaged-foundry", "hasVision", {
        name: "Give imported character tokens vision:",
        hint: "Enables vision for character's tokens during import. This does not affect characters imported as NPCs.",
        type: Boolean,
        default: true,
        scope: "world",
        config: true
    })

    game.settings.register("savaged-foundry", "addImage", {
        name: "Add full size image to bottom of description:",
        hint: "Your characters deserve to be seen, but we understand if you want your space.",
        type: Boolean,
        default: true,
        scope: "world",
        config: true
    })

    game.settings.register("savaged-foundry", "zDebug", {
        name: "Debug:",
        hint: "Toggles output of debugging info to the browser console.",
        type: Boolean,
        default: false,
        scope: "world",
        config: true
    })
}
