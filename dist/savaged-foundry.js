// Import JavaScript modules
import {registerSettings} from './module/settings.js'
import {preloadTemplates} from './module/preloadTemplates.js'
import {importCharacterData} from './module/coreFunctions.js'
import {logger, setLogLevel, LOG_LVL} from "./module/log.js"
import {fetchCharacters, fetchThisCharacter, searchBestiary, testConnection} from "./module/api.js"
import {getSelectedOptions, getCurrentActorsUUID, sortCharacters} from "./module/utils.js"
import SavagedImport from "./module/savagedImport.js"

//CONFIG.debug.hooks = true

/* ------------------------------------ */
/* Initialize module					*/
/* ------------------------------------ */
Hooks.once('init', async function () {
    logger(LOG_LVL.INFO, ' | Initializing savaged-foundry...');

    // Assign custom classes and constants here

    // Register custom module settings
    registerSettings();
    // Preload Handlebars templates
    await preloadTemplates();

    // Register custom sheets (if any)
    logger(LOG_LVL.INFO, ' | Done initializing...')
})

/* ------------------------------------ */
/* Setup module							*/
/* ------------------------------------ */
Hooks.once('setup', function () {
    // Do anything after initialization but before
    // ready
})

/* ------------------------------------ */
/* When ready							*/
/* ------------------------------------ */
Hooks.once('ready', function () {
    // Do anything once the module is ready
    logger(LOG_LVL.INFO, " | Ready!!")
    logger(LOG_LVL.DEBUG, " settings are : " + JSON.stringify(game.settings))
})

// characters: incoming import data
// uuidLookup: hashmap of already imported characters in foundry by uuid


// TODO: fix file import images and thumbnail
Hooks.on('renderActorDirectory', (app, html, data) => {
    if (game.users.get(game.userId).can('ACTOR_CREATE')) {
        const footer = document.createDocumentFragment()
        const systemVersion = parseFloat(game.data.version.split('.').slice(0,2).join(''))
        if ( systemVersion >= 7 ) { // for forward/backward compatibility around 6.x and 7.x versions of foundry
            const hrule = document.createElement("hr")
            hrule.style = "border: 1px solid black;"
            footer.appendChild(hrule)
        }
        const headerDiv = document.createElement("div")
        headerDiv.style = "min-width: 96%; text-align: center; padding-top: 2px;"
        const headerText = document.createTextNode(" Savaged.US Import ")
        const savagedLink = document.createElement("a")
        savagedLink.href = "https://gitlab.com/tenuki.igo/savaged-foundry/-/wikis/How-to-Import-a-Character"
        const linkIcon = document.createElement("i"); linkIcon.setAttribute("class", "fa fa-info-circle");
        savagedLink.appendChild(linkIcon)
        const toggleLabel = document.createElement("span"); toggleLabel.style = "padding-left: 5px;"; toggleLabel.innerText = 'API'
        const toggle = document.createElement("i"); toggle.setAttribute("class", "fas fa-toggle-off"); toggle.style = "padding-left: 20px;"
        headerDiv.appendChild(headerText); headerDiv.appendChild(savagedLink); headerDiv.appendChild(toggle); headerDiv.appendChild(toggleLabel)
        footer.appendChild(headerDiv)
        const loading = $('<i id="loadingSpinner" style="display: none;" class="fas fa-spinner fa-spin"></i>')[0]
        const fileImportButton = $('<button  style="color: green; max-width: 48%;  min-width: 48%; margin: 5px 2px;">File</button>')
        const apiImportButton = $('<button  style="max-width: 48%; min-width: 48%; margin: 5px 2px;">Character</button>')
        const bestiaryImportButton = $('<button style="max-width: 48%;  min-width: 48%; margin: 5px 2px;">Bestiary</button>')
        const clearAPIKey = $('<button  style="max-width: 48%;  min-width: 48%; margin: 5px 2px;">Clear APIKey</button>')
        //TODO: default buttons so people can hit enter to import or search
        toggleAPI()
        html.find('.directory-footer').append(footer, fileImportButton, apiImportButton, bestiaryImportButton, clearAPIKey)

        bestiaryImportButton.click(ev => {
            logger(LOG_LVL.INFO, "got here -tk")
            let selectedBeasts = []
            let searchString = ""
            let content = '<h1>Search the Bestiary</h1>'
            content += '<input type="text" style="display: inline; width: 50%" value="" name="searchString" id="searchString">'
            content += '<button style="display: inline; width: 25%; padding-right: 10px;" id="beastSearch">Search</button>'
            content += loading.outerHTML
            loading.style.display = ''
            content += '<select multiple style="min-height: 200px; min-width: 100%;" id="beastList"></select>'
            let savagedInPrompt = new SavagedImport({
                title: "Savaged Foundry - Bestiary",
                content: content,
                buttons: {
                    "Cancel": {
                        icon: '',
                        label: "Cancel",
                        callback: () => {
                        }
                    },
                    "Import": {
                        icon: '',
                        label: "Import",
                        callback: () => {
                            selectedBeasts = getSelectedOptions(document.getElementById('beastList'))
                            searchString = SavagedImport.search_string
                            logger(LOG_LVL.DEBUG, "Selected: " + selectedBeasts)
                        }
                    }
                },
                close: async () => {
                    // Here is where the magic happens.
                    selectedBeasts.forEach(async (selectedBeast) => {
                        let beastData = SavagedImport.beasts.find(beast => beast.uuid == selectedBeast)
                        logger(LOG_LVL.DEBUG, "importing " + JSON.stringify(beastData))
                        await Actor.create(importCharacterData(beastData, {method: 'API'}, 'npc'))
                        logger(LOG_LVL.INFO, "Character " + selectedBeast + " created!")
                    })
                }
            }, {popout: true, width: 540, resizable: false})
            savagedInPrompt.render(true)
        })

        fileImportButton.click(ev => {
            logger(LOG_LVL.DEBUG, "file import button pressed")
            let files = []
            let content = '<h1>Generic JSON File Import</h1> '
                + '<input class="file-picker" type="file" id="savaged-json" accept=".json" multiple name="savaged-json">'
                + '<hr>'
                + '<div class="savagedus-file-import-group"></div>'
            let savagedInPrompt = new Dialog({
                title: "Savaged Foundry - generic JSON Files",
                content: content,
                buttons: {
                    "Import": {
                        icon: '<i class="fas fa-times"></i>',
                        label: "Import",
                        callback: async (e) => {
                            files = document.querySelector('#savaged-json').files
                            if (files) {
                                for (let i = 0; files.length; i++) {
                                    logger(LOG_LVL.DEBUG, "importing " + JSON.stringify(files))
                                    const reader = new FileReader()
                                    reader.onload = async (e) => {
                                        const characterData = JSON.parse(e.target.result)
                                        // Here is where the magic happens.
                                        let select = document.getElementById('import-type-'+characterData.uuid)
                                        let inType = select.value
                                        if ((characterData.playerCharacter === true || characterData.rankName !== 'Bestiary') && inType === 'npc') {
                                            characterData.bennies = characterData.bennies - 1
                                            characterData.benniesMax = characterData.benniesMax - 1
                                        }
                                        if ((characterData.playerCharacter === false || characterData.rankName === 'Bestiary') && (characterData.wildcard === true || characterData.wildcard === 1) && inType === 'character') {
                                            characterData.bennies = characterData.bennies + 1
                                            characterData.benniesMax = characterData.benniesMax + 1
                                        }
                                        await Actor.create(importCharacterData(characterData, {method: 'File'}, inType))
                                        logger(LOG_LVL.INFO, "Character " + files[i] + " created!")
                                    }
                                    await reader.readAsText(files[i])
                                }
                            } else {
                                logger(LOG_LVL.INFO, "no files selected...")
                            }
                        }
                    },
                    "Cancel": {
                        icon: '',
                        label: "Cancel",
                        callback: () => {
                        }
                    }
                },
                render: html => {
                    content = ''
                    let fileInput = document.querySelector('#savaged-json')
                    let importSelectionsList = document.querySelector('.savagedus-file-import-group')
                    let characterFileData = {}
                    fileInput.addEventListener('change', async (event) => {
                        files = event.target.files
                        for (let i = 0, file; file = files[i]; i++) {
                            let preview = new FileReader()
                            preview.onload = (function (theFile) {
                                return function (e) {
                                    characterFileData = JSON.parse(e.target.result)
                                    logger(LOG_LVL.INFO, "Character " + characterFileData.name + " selected")
                                    let label = document.createElement('label')
                                    let select = document.createElement('select')
                                    let option1 = document.createElement('option')
                                    let character = document.createTextNode('Character')
                                    let option2 = document.createElement('option')
                                    let npc = document.createTextNode('NPC')
                                    let selectID = 'import-type-' + characterFileData.uuid
                                    select.id = selectID
                                    label.setAttribute('for', selectID)
                                    label.innerText = characterFileData.name
                                    option1.value = character.wholeText.toLowerCase()
                                    option1.appendChild(character)
                                    option2.value = npc.wholeText.toLowerCase()
                                    option2.appendChild(npc)
                                    if (characterFileData.playerCharacter === true || characterFileData.rankName !== 'Bestiary') {
                                        option1.setAttribute('selected','')
                                    } else {
                                        option2.setAttribute('selected','')
                                    }
                                    select.append(option1, option2)
                                    importSelectionsList.append(label, select)
                                }
                            })(file)
                            preview.readAsText(file)
                        }
                        importSelectionsList.closest('.app.window-app.dialog').style.height = 'auto'
                        importSelectionsList.closest('.app.window-app.dialog').style.maxHeight = '90vh'
                    })
                },
                close: async () => {
                }
            })
            savagedInPrompt.render(true)
        })

        // Button for API Import handling
        apiImportButton.click(async (ev) => {
            logger(LOG_LVL.DEBUG, "api import button pressed")
            let api_key = localStorage.getItem("api-key")
            loading.style.display = ''
            let content = '<div id="charList" style="height: 300px;">' + loading.outerHTML + '</div>'
                // + '<div><i><b>Bolded</b> characters have already been imported and will be left alone.  <b style="text-shadow: 1px 1px 1px green;">'
                // + 'Bolded with green outline</b> have been updated on savaged.us and will be synced with the previously imported character.</i><hr>'
                + '<input type="checkbox" disabled id="dupes" name="dupes"/><label style="vertical-align: top" for="dupes">Import a new character instead of updating.</label></div>'
            let message = '<h1 style="min-width: 550px;" id="messageText">Character Import</h1>'
            if (api_key) {
                let savagedInPrompt = new Dialog({
                    title: "Savaged Foundry - Characters",
                    content: message + content, // + characterSelect,
                    buttons: {
                        "Import": {
                            icon: '<i class="fas fa-times"></i>',
                            label: "Import",
                            callback: (e) => {
                                selectedCharacters = getSelectedOptions(document.getElementById('characterList'))
                                logger(LOG_LVL.DEBUG, "Selected: " + selectedCharacters)
                            }
                        },
                        "Cancel": {
                            icon: '',
                            label: "Cancel",
                            callback: () => {
                            }
                        }
                    },
                    close: async () => {
                        // Here is where the magic happens.
                        for (let i = 0; i < selectedCharacters.length; ++i) {
                            let characterData = characters.find(character => character.uuid == selectedCharacters[i])
                            logger(LOG_LVL.DEBUG, "importing " + JSON.stringify(characterData))
                            let uuidLookup = getCurrentActorsUUID(game.actors)
                            let actor = {}
                            let uuid = ''
                            if (typeof characterData !== 'undefined') {
                                uuid = characterData.uuid
                            }
                            // update
                            if (uuid !== '' && uuid in uuidLookup) {
                                if (uuidLookup[uuid].updated != characterData.updatedDate) {
                                    let act = game.actors.get(uuidLookup[uuid].id)
                                    actor = await act.update(importCharacterData(characterData, {method: 'API'}))
                                } else {
                                    //noop
                                    logger(LOG_LVL.ERROR, "Didn't find character in current list - this should not happen")
                                }
                                // new
                            } else {
                                actor = await Actor.create(importCharacterData(characterData, {method: 'API'}))
                            }
                            logger(LOG_LVL.INFO, "Character " + selectedCharacters[i] + " created!")
                            logger(LOG_LVL.DEBUG, "imported: " + JSON.stringify(actor))
                        }
                    }
                })
                savagedInPrompt.render(true)
                let characters = await fetchCharacters(api_key)
                loading.style.display = 'none'
                let selectedCharacters = []

                let characterSelect = '<select style="height: 300px; width: 98%; font-size: medium;" name="characterList" id="characterList" multiple>'
                if (characters.length > 0) {
                    let uuidLookup = getCurrentActorsUUID(game.actors)
                    let sortedCharacters = sortCharacters(characters, uuidLookup)
                    Object.keys(sortedCharacters).forEach(key => {
                        characterSelect += '<optgroup label="' + key + '">'
                        sortedCharacters[key].forEach(char => {
                            let styleText = "font-weight: lighter; color: grey;"
                            let disable = " disabled"
                            if (uuidLookup[char.uuid]) {
                                if (uuidLookup[char.uuid].updated != char.updatedDate) {
                                    styleText = 'font-weight: bold; text-shadow: 1px 1px 1px yellow;'
                                    disable = ""
                                }
                                characterSelect += '<option'+ disable +' style="' + styleText + '" value="' + char.uuid + '">'
                            } else {
                                characterSelect += '<option style="color: darkgreen; font-weight: bold;" value="' + char.uuid + '">'
                            }
                            characterSelect += char.name + ' --- '
                                + char.rankName + ' ' + char.gender + ' ' + char.race + ' ' + char.professionOrTitle
                                + ', saved on ' + char.updatedDate + '</option>'
                        })
                        characterSelect += '</optgroup>'
                    })
                } else {
                    message = "<b>You have no characters to import!!</b>"
                }
                characterSelect += '</select>'
                document.getElementById("charList").outerHTML = characterSelect
            }
        })

        clearAPIKey.click(async ev => {
            let content = "This will remove your key from local storage, turning off the ability to import.  Are you sure?"
            if (localStorage.getItem('api-key')) {
                let savagedInPrompt = Dialog.confirm({
                    title: "Savaged Foundry - API key Managment",
                    content: content,
                    yes: () => {
                        localStorage.removeItem('api-key')
                        toggleAPI()
                    },
                    no: () => {
                        //
                    },
                    defaultYes: false
                })
                logger(LOG_LVL.DEBUG, "clear api key button pressed")

            } else {
                content = 'Please enter <a href="https://savaged.us/me/api-key">API Key from Savaged.US</a><input id="api-key" type="password" name="api-key">.'
                    + ' Once this is set you will be able to use the same button to import characters from your account there.'
                let savagedInPrompt = new Dialog({
                    title: "Savaged Foundry - API key Managment",
                    content: content,
                    buttons: {
                        "Save": {
                            icon: '<i class="fas fa-times"></i>',
                            label: "Save",
                            callback: async (e) => {
                                let api_key = document.getElementById('api-key').value
                                let test = await testConnection(api_key)
                                console.log("-tk returned from testConnection(): ", test)
                                if (test) {
                                    toggleAPI()
                                }
                                console.log("-tk test: ", test)
                            }
                        },
                        "Cancel": {
                            icon: '',
                            label: "Cancel",
                            callback: () => {
                            }
                        }
                    },
                    close: async () => {
                    }
                })
                savagedInPrompt.render(true)
            }
            toggleAPI()
        })

        function toggleAPI() {
            if (localStorage.getItem('api-key')) {
                toggle.style.color = 'green'
                toggle.className = 'fas fa-toggle-on'
                clearAPIKey[0].innerText = 'Clear API Key'
                apiImportButton[0].disabled = false
                apiImportButton[0].style.color = 'green'
                bestiaryImportButton[0].disabled = false
                bestiaryImportButton[0].style.color = 'green'
            } else {
                toggle.style.color = 'red'
                toggle.className = 'fas fa-toggle-off'
                clearAPIKey[0].innerText = 'Set API Key'
                apiImportButton[0].disabled = true
                apiImportButton[0].style.color = '#888888'
                bestiaryImportButton[0].disabled = true
                bestiaryImportButton[0].style.color = '#888888'
            }
        }
    }
})

Hooks.on('renderSwadeCharacterSheet', (app, html, data) => {
    let api_key = localStorage.getItem("api-key")
    if (api_key) {
        if (typeof data.actor.flags.importInfo !== 'undefined') {
            if (typeof data.actor.flags.importInfo.uuid !== 'undefined') {
                let uuid = data.actor.flags.importInfo.uuid
                let fvttId = data.actor._id
                let actorName = data.actor.name
                let actor = {}
                let charNameHeading = $('#actor-' + fvttId + '.swade.sheet.actor .sheet-header .charline')
                const loading = $('<i id="loadingSpinner" style="display: none;" class="fas fa-spinner fa-spin"></i>')[0]
                let charImporterButton = $('<button class="savagedus-logo-button"><img class="" src="modules/savaged-foundry/assets/svgd-us.png"/></button>')
                charNameHeading.prepend(charImporterButton)
                charImporterButton.click(async ev => {
                    //A second check in case the API key is reset while the actor sheet is open.
                    api_key = localStorage.getItem("api-key")
                    if (api_key) {

                        loading.style.display = ''
                        logger(LOG_LVL.DEBUG, "api character import button pressed")
                        let savagedInPrompt = new Dialog ({
                            title: 'Savaged Foundry - Update ' + actorName,
                            content: '<p>Checking ' + actorName + ' for updates. ' + loading.outerHTML + '</p>',
                            buttons: {}
                        })
                        savagedInPrompt.render(true)
                        let characterData = await fetchThisCharacter(api_key, uuid)
                        savagedInPrompt.close(true)
                        if (!(characterData === null)) {    
                            let confirmationMessage = ''
                            if (data.actor.flags.importInfo.updated != characterData.updatedDate) {
                                savagedInPrompt = Dialog.confirm({
                                    title: 'Savaged Foundry - Update ' + actorName,
                                    content: '<p>An update is available for ' + actorName + '. Would you like to import the update?</p>',
                                    yes: () => {
                                        confirmationMessage = '<p>' + actorName + ' has been updated!</p>'
                                        let act = game.actors.get(fvttId)
                                        actor = act.update(importCharacterData(characterData, {method: 'API'}))
                                        savagedInPrompt = new Dialog({
                                            title: 'Savaged Foundry - ' + actorName + ' Updated',
                                            content: confirmationMessage,
                                            buttons: {
                                                "Close": {
                                                    icon: '',
                                                    label: "Close",
                                                    callback: () => {
                                                    }
                                                }
                                            }
                                        })
                                        savagedInPrompt.render(true)
                                        ChatMessage.create({content: '<p>' + actorName + ' has been updated!</p>'})
                                    },
                                    no: () => {},
                                    defaultYes: false
                                })
                            } else {
                                savagedInPrompt = Dialog.confirm({
                                    title: 'Savaged Foundry - No Updates for ' + actorName,
                                    content: '<p>' + actorName + ' has no update available. Would you like to refresh the character anyway?</p>',
                                    yes: () => {
                                        confirmationMessage = '<p>' + actorName + ' has been refreshed.</p>'
                                        let act = game.actors.get(fvttId)
                                        actor = act.update(importCharacterData(characterData, {method: 'API'}))
                                        savagedInPrompt = new Dialog({
                                            title: 'Savaged Foundry - ' + actorName + ' Updated',
                                            content: confirmationMessage,
                                            buttons: {
                                                "Close": {
                                                    icon: '',
                                                    label: "Close",
                                                    callback: () => {
                                                    }
                                                }
                                            }
                                        })
                                        savagedInPrompt.render(true)
                                        ChatMessage.create({content: confirmationMessage})
                                    },
                                    no: () => {},
                                    defaultYes: false
                                })
                            }
                        } else {
                            let savagedInPrompt = new Dialog({
                                title: 'Savaged Foundry - ' + actorName + ' Not Found',
                                content: '<p>Cannot retrieve character. Either the character has been deleted or the API key is incorrect.</p><ul><li>Character Name: <code>' + actorName + '</code></li><li>UUID: <code>' + uuid + '</code></li></ul>',
                                buttons: {
                                    "Close": {
                                        icon: '',
                                        label: "Close",
                                        callback: () => {
                                        }
                                    }
                                }
                            })
                            savagedInPrompt.render(true)
                        }
                    }
                })
            }
        }
    }
})

Hooks.on('closeExtendedSettingsConfig', (app, html, data) => {
    // on settings set debug level
    logger(LOG_LVL.DEBUG, "Settings possibly changed.")
    if (game.settings.get("savaged-foundry", "zDebug"))
        setLogLevel(LOG_LVL.DEBUG)
    else
        setLogLevel(LOG_LVL.INFO)  // for now
})
